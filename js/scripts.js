window.onload = async function run() {
    $('.loading_icon').append(`
    <div class="lds-dual-ring"></div>
    `);
    const response = await fetch(api);
    movies = await response.json();

    $('#loading_icon').empty();

    this.loadGrid(movies);
}

async function loadGrid(movies) {
    $('.loading_icon').append(`
    <div class="lds-dual-ring"></div>
    `);
    $('#grid_view').empty();
    var str = '';
    var row = 0;
    for (var index = 0; index < movies.length; index++) {
        row = Math.floor(index / 3);
        $('#grid_view').append(`
        <div class="row" id="row_${row}">
        </div>
        `);
        str = '#row_' + row.toString();
        var releaseDate = movies[index].release_date.split("-");
        var movie_id = movies[index].id;
        $(str).append(`
        <div class="col-lg-4 mb-4">
            <div class="card h-100" style="width: 18rem; cursor: pointer;">
                <a id="movie_${index}" onclick="gotoMovieDetails('${movie_id}')">
                    <img class="card-img-top" src="${movies[index].poster}" alt="${movies[index].title}">
                    <div class="card-body">
                        <h4 class="card-title font-weight-bold">${movies[index].title}</h4>
                        <div class="divider-custom">
                            <div class="divider-custom-line-title"></div>
                        </div>
                        <h6 class="card-text">Director: ${movies[index].director}</h6>
                        <h6 class="card-text">Release date: ${releaseDate[2]}/${releaseDate[1]}/${releaseDate[0]}</h6>
                        <h6 class="card-text">IMDB: ${movies[index].imdb}/10</h6>
                        <h6 class="card-text">Runtime: ${movies[index].runtime} mins</h6>
                    </div>
                </a>
            </div>
        </div>
        `);
    }
    $('.loading_icon').empty();
}

function gotoMovieDetails(id) {
    localStorage.setItem('id', id);
    window.open("./movie_details.html", "_self");
}

function searchFilter(input) {
    const movie_result = movies.filter(movie => {
        return movie.title.toLowerCase().search(input) > -1;
    });
    const actor_result = movies.filter(movie => {
        for (const one_actor of movie.actor) {
            if (one_actor.name.toLowerCase().search(input) > -1)
                return one_actor.name.toLowerCase().search(input) > -1;
        }
    });
    const director_result = movies.filter(movie => {
        return movie.director.toLowerCase().search(input) > -1;
    });
    var result = movie_result.concat(actor_result).concat(director_result);

    var count = 0;
    var output = [];
    var start = false;
    for (j = 0; j < result.length; j++) {
        for (k = 0; k < output.length; k++) {
            if (result[j] == output[k]) {
                start = true;
            }
        }
        count++;
        if (count == 1 && start == false) {
            output.push(result[j]);
        }
        start = false;
        count = 0;
    }
    return output;
}

async function submitSearch() {
    var search_input = $('#searchInput').val()
    var search = search_input.toLowerCase();
    $('.loading_icon').append(`
    <div class="lds-dual-ring"></div>
    `);
    $('#grid_view').empty();
    const result = searchFilter(search);
    await sleep(300);
    $('.loading_icon').empty();
    if (Array.isArray(result) && result.length) {
        if (search_input) {
            $('#view_side').empty();
            $('#view_side').append(`
            <h5 class="text-center font-italic">Search results for: '${search_input}'</h5>
            `);
        }
        else {
            $('#view_side').empty();
        }
        this.loadGrid(result);
    } else {
        $('#view_side').empty();
        $('#grid_view').empty();
        $('#grid_view').append(`
        <h5 class="text-center font-italic">No results were found</h5>
        `);
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}