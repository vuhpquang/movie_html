window.onload = async function run() {
    const response = await fetch(api);
    movies = await response.json();
    

    $('#details').empty();

    var movie = movies.find(movie => {
        return movie.id == this.id;
    })
    var releaseDate = movie.release_date.split("-");

    const response_tmdb = await fetch(movieDetails+movie.tmdb_id+'/reviews?'+key);
    var tmdbReviews = await response_tmdb.json();
    tmdbReviews = tmdbReviews.results;

    $('#details').append(`
    <div class="col-md-3 text-center">
        <img width="417px" heigth="617px" class="img-fluid my-4" src="${movie.poster}" alt="${movie.title}">
        <div id="actor_popup">
                <button type="button" class="btn btn-primary portfolio-item mx-auto pointer" data-toggle="modal" data-target="#portfolioModalReviews">
                    View Reviews
                </button>
                <div class="portfolio-modal modal fade" id="portfolioModalReviews" tabindex="-1" role="dialog"
                    aria-labelledby="portfolioModal1Label" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </span>
                            </button>
                            <div class="modal-body text-center">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <!-- Portfolio Modal - Title -->
                                            <h2 class="portfolio-modal-title text-primary text-uppercase mb-0 display-3">Reviews</h2>
                                            <!-- Icon Divider -->
                                            <div class="divider-custom">
                                                <div class="divider-custom-line"></div>
                                                <div class="divider-custom-icon">
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="divider-custom-line"></div>
                                            </div>

                                            <!-- Portfolio Modal - Text -->
                                            <div class="mb-3 mt-3" id="review_list">
                                                
                                            </div>
                                            <button class="mt-3 btn btn-primary" href="#" data-dismiss="modal">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                Close Window
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
    </div>
    <div class="col-md-9">
        <h1 class="my-4 text-uppercase">${movie.title}</h1>
        <h3 class="my-3">Story line</h3>
        <p>${movie.storyline}</p>
        <h3 class="my-3">Movie Details</h3>
        <ul>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>Director:</p>
                        </div>
                        <div class="col-sm-8">
                            <h6>${movie.director}</h6>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>Genres:</p>
                        </div>
                        <div class="col-sm-8">
                            <h6>${movie.genres}</h6>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>Release date:</p>
                        </div>
                        <div class="col-sm-8">
                            <h6>${releaseDate[2]}/${releaseDate[1]}/${releaseDate[0]}</h6>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>Runtime:</p>
                        </div>
                        <div class="col-sm-8">
                            <h6>${movie.runtime} minutes</h6>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p>IMDB:</p>
                        </div>
                        <div class="col-sm-8">
                            <h6><i id="imdb" class="fa fa-star mr-1" aria-hidden="true"></i>${movie.imdb}/10<i id="imdb" class="fa fa-star ml-1" aria-hidden="true"></i></h6>
                        </div>
                    </div>
                </div>
            </li>
            <li>
                <div class="container">
                    <div class="row">
                        <div class="col-sm-4">
                            <p id="test">Actors:</p>
                        </div>
                        <div class="col-sm-8">
                            <ul id="actorList"></ul>
                        </div>
                    </div>
                </div>
            </li>
        </ul>
    </div>
    `);

    for (const tmdbReview of tmdbReviews)  {
        var bold = /\*\*(\S(.*?\S)?)\*\*/gm;
        var bold_italic = /\*\*\*(\S(.*?\S)?)\*\*\*/gm;
        var italic = /\_(\S(.*?\S)?)\_/gm;
        var content_review = tmdbReview.content.toString().replace(bold_italic,'<span class="font-italic font-weight-bold">$1</span>');
        content_review = content_review.replace(bold,'<span class="font-weight-bold">$1</span>');
        content_review = content_review.replace(italic,'<span class="font-italic">$1</span>');
        $('#review_list').append(`
        <div class="container">
            <blockquote class="blockquote">
                <p class="mb-0">${content_review}</p>
                <footer class="blockquote-footer">${tmdbReview.author} - <cite title="Source Title"><a href="${tmdbReview.url}" target="_blank">TheMovieDB</a></cite></footer>
            </blockquote>
            <div class="divider-custom">
                <div class="divider-custom-line-title"></div>
            </div>
        </div>
        `);
    }
    $('#test').append(`
    <div class="loading_icon w-25 p-3"></div>
    `);
    $('.loading_icon').append(`
    <div class="lds-dual-ring"></div>
    `);

    for (const actor of movie.actor) {
        var actor_response = await fetch(actorDetails+actor.id_actor_tmdb+'?'+key);
        var actorDe = await actor_response.json();
        var movies_appeared = movieAppeared(actor.name.toLowerCase());
        var currentTime = new Date();
        var avatar_link = 'https://image.tmdb.org/t/p/w500' + actorDe.profile_path.toString();

        var age = (parseInt(currentTime.getFullYear()) - parseInt(actor.birth.substring(actor.birth.length, actor.birth.length - 4))).toString();
        $('#actorList').append(`
        <li class="d-flex flex-row">
            <div id="actor_popup">
                <h6 class="portfolio-item mx-auto pointer" data-toggle="modal" data-target="#portfolioModal${actor.id_actor}">
                    <i class="fa fa-hand-o-right mr-2" aria-hidden="true"></i>${actor.name}
                </h6>
                <div class="portfolio-modal modal fade" id="portfolioModal${actor.id_actor}" tabindex="-1" role="dialog"
                    aria-labelledby="portfolioModal1Label" aria-hidden="true">
                    <div class="modal-dialog modal-xl" role="document">
                        <div class="modal-content">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">
                                    <i class="fa fa-times" aria-hidden="true"></i>
                                </span>
                            </button>
                            <div class="modal-body text-center">
                                <div class="container">
                                    <div class="row justify-content-center">
                                        <div class="col-lg-8">
                                            <!-- Portfolio Modal - Title -->
                                            <h2 class="portfolio-modal-title text-primary text-uppercase mb-0 display-3">${actor.name}</h2>
                                            <!-- Icon Divider -->
                                            <div class="divider-custom">
                                                <div class="divider-custom-line"></div>
                                                <div class="divider-custom-icon">
                                                    <i class="fa fa-star-o" aria-hidden="true"></i>
                                                </div>
                                                <div class="divider-custom-line"></div>
                                            </div>

                                            <!-- Portfolio Modal - Image -->
                                            <img class="avatar img-fluid rounded" src="${avatar_link}" alt="${actor.name}">
                                            
                                            <!-- Portfolio Modal - Text -->
                                            <div class="mb-3 mt-3">
                                                <center mb-3><h4>Born: ${actor.birth} (age ${age})</h4></center>
                                                <center><h3>Biography</h3></center>
                                                <p>
                                                    ${actorDe.biography}
                                                </p>
                                            </div>
                                            <center><h3>Movies</h3></center>
                                            <div class="row" id="movie_appeared_${actor.id_actor}">
                                                
                                            </div>
                                            <button class="mt-3 btn btn-primary" href="#" data-dismiss="modal">
                                                <i class="fa fa-times" aria-hidden="true"></i>
                                                Close Window
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </li>
        `);
        for (const movie_appeared of movies_appeared) {
            var releaseDateAppeared = movie_appeared.release_date.split("-");
            $('#movie_appeared_'+actor.id_actor.toString()).append(`
            <div class="mx-1 my-1">
                <div class="card h-100" style="width: 10rem; cursor: pointer;">
                    <a id="movie_${0}" onclick="gotoMovieDetails('${movie_appeared.id}')">
                        <img class="card-img-top" src="${movie_appeared.poster}" alt="${movies_appeared.title}">
                        <div class="card-body">
                            <h4 class="card-title font-weight-bold">${movie_appeared.title}</h4>
                            <div class="divider-custom">
                                <div class="divider-custom-line-title"></div>
                            </div>
                            <h6 class="card-text">Director: ${movie_appeared.director}</h6>
                            <h6 class="card-text">Release date: ${releaseDateAppeared[2]}/${releaseDateAppeared[1]}/${releaseDateAppeared[0]}</h6>
                            <h6 class="card-text">IMDB: ${movie_appeared.imdb}/10</h6>
                            <h6 class="card-text">Runtime: ${movie_appeared.runtime}s</h6>
                        </div>
                    </a>
                </div>
            </div>
            `);
        }
    }
    $('.loading_icon').empty();
}

function gotoMovieDetails(id) {
    localStorage.setItem('id', id);
    window.open("./movie_details.html", "_self");
}

function movieAppeared(input) {
    const actor_result = movies.filter(movie => {
        for (const one_actor of movie.actor) {
            if (one_actor.name.toLowerCase().search(input) > -1)
                return one_actor.name.toLowerCase().search(input) > -1;
        }
    });
    const director_result = movies.filter(movie => {
        return movie.director.toLowerCase().search(input) > -1;
    });
    var result = actor_result.concat(director_result);

    var count = 0;
    var output = [];
    var start = false;
    for (j = 0; j < result.length; j++) {
        for (k = 0; k < output.length; k++) {
            if (result[j] == output[k]) {
                start = true;
            }
        }
        count++;
        if (count == 1 && start == false) {
            output.push(result[j]);
        }
        start = false;
        count = 0;
    }
    return output;
}

var id = localStorage.getItem('id');